# Ansible course setup

This repository contains all the scripts and files necessary to configure a machine for the Instil Ansible training course.

Setup used Vagrant to provision the machines required for the course, it is therefore necessary for you to have Vagrant installed on the computer you wish to use during the training course. Please refer to [Vagrant site](http://vagrantup.com) for details on installing Vagrant.

With Vagrant installed on your machine, simply:

* Clone this repository - ```git clone https://gitlab.com/instil_training/ansible-setup.git```
* Change to the directory containing the scripts - ```cd ansible-setup```
* Run the Vagrant up command - ```vagrant up```

Vagrant will now standup four virtual machines: controller, dbserver, apiserver and webserver. The setup scripts also take care of provisioning ssh keys and manage all other configuration required on the four virtual machines.

*Note:* Provisioning of these four machines can take some time, especially if this is the first time you've used Vagrant on your computer.

You will be using the controller machine for the rest of the course as your Ansible controller. This is the machine from which you will be issuing all Ansible commands which will be run against the other three machines.

To test that the Vagrant provisioning has succeeded:

* SSH onto the controller machine. The easiest way to do this is to issue the command: ```vagrant ssh controller```
* From the controller machine you can ping all the machines by name (there should be entries for each machine in the ```/etc/hosts``` file of the controller machine).
